### **Simple python script to get list of AWS LoadBalancers, TargetGroups and Listeners**
____
Script is develped for Python 3.6
____
How to use:
1. Install requirements ```pip install -r requirements.txt```
2. Configure AWS CLI. 
   
    If you don't know how - just follow the instructions here https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html
3. Run the script using command ```python main.py```
4. See the received data in ```output.json```