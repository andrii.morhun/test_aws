import boto3
import json

def main():
    client = boto3.client('elbv2')
    load_balancers_json = client.describe_load_balancers()
    load_balancers_list = load_balancers_json["LoadBalancers"]
    output_data = []
    for balancer in load_balancers_list:
        balancer_arn = balancer["LoadBalancerArn"]
        balancer_listeners_arns = get_listeners(balancer_arn, client)
        balancer_target_groups_arns = get_targets(balancer_arn, client)

        balacer_dict = {
            "LoadBalancerArn": balancer_arn,
            "ListenerArns": balancer_listeners_arns,
            "TargetGroupArns": balancer_target_groups_arns,
        }
        output_data.append(balacer_dict)
    with open("output.json", 'w') as outfile:
        json.dump(output_data, outfile, indent=4)


def get_targets(balancer_arn, client):
    # get targets for each balancer
    balancer_targets_json = client.describe_target_groups(
        LoadBalancerArn=balancer_arn)
    balancer_targets_arns = []
    if balancer_targets_json:
        balancer_targets_list = balancer_targets_json['TargetGroups']
        for target in balancer_targets_list:
            balancer_targets_arns.append(target["TargetGroupArn"])
    return balancer_targets_arns


def get_listeners(balancer_arn, client):
    # get listeners for each balancer
    balancer_listeners_json = client.describe_listeners(
        LoadBalancerArn=balancer_arn)
    balancer_listeners_arns = []
    if balancer_listeners_json:
        balancer_listeners_list = balancer_listeners_json['Listeners']
        for listener in balancer_listeners_list:
            balancer_listeners_arns.append(listener["ListenerArn"])
    return balancer_listeners_arns


if __name__ == "__main__":
    main()